
# Create Application

Using Quarkus \([https://quarkus.io/](https://quarkus.io/)\), develop a CRUD REST API for the following models:

### Post

* id \(PK\)
* title
* content
* tags \(many-to-many to tag\)

### Tag

* id \(PK\)
* label
* posts \(many-to-many to post\)

## REQUIRMENTs

* JDK 11
* Database : Postgresql
  * DBNAME : testctcorp
  * USERDB : postgres
  * PASSDB : 
  * PORTDB : 5432

DOCUMENTATION

* [https://documenter.getpostman.com/view/12171226/TzJsfHqi\#intro](https://documenter.getpostman.com/view/12171226/TzJsfHqi#intro)
