package org.ctgroup.test.services;

import java.util.List;

import javax.persistence.Entity;

import org.ctgroup.test.entity.Post;
import org.ctgroup.test.entity.Tag;

public interface ApplicationService {

	//save
	public void save(Post post);
	public void save(Tag tag);
	
	//get data
	public List<Post> findAllPosts();
	public List<Tag> findAllTags();
	
	//update
	public void updatePost(Post post);
	public void updateTag(Tag tag);

	//delete
	public void deletePost(Long post);
	public void deleteTag(Long tag);
}
