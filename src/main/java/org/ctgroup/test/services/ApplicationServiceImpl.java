package org.ctgroup.test.services;

import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.ctgroup.test.dao.ApplicationDaoImpl;
import org.ctgroup.test.entity.Post;
import org.ctgroup.test.entity.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//@Service
@ApplicationScoped
@Transactional
public class ApplicationServiceImpl implements ApplicationService{
	
	Logger logger = LoggerFactory.getLogger(ApplicationServiceImpl.class);
	
	@Inject
	ApplicationDaoImpl dao;
	
	public void save(Post post) {
		// TODO Auto-generated method stub
		post.setCreateAt(new Date());
		dao.savePost(post);
		logger.info("Data Post Saved.");
	}
	
	public void save(Tag tag) {
		tag.setCreateAt(new Date());
		dao.saveTag(tag);
		logger.info("Data Tag Saved.");
	}
	
	public void updatePost(Post post) {
		Post cPost = dao.getPostById(post.getId());
		if(cPost != null) {
			post.setUpdateAt(new Date());
			dao.merge(post);
		}
		logger.info("Data Post "+ post.getTitle() + ", UPDATED");
	}
	
	public void updateTag(Tag tag) {
		Tag cTag = dao.getTagById(tag.getId());
		if(cTag != null) {
			tag.setUpdateAt(new Date());
			dao.merge(tag);
		}
		logger.info("Data Tag "+ tag.getLabel()+ ", UPDATED");
	}
	
	public List<Post> findAllPosts() {
		// TODO Auto-generated method stub
		List<Post> posts = dao.findAllPosts();
		
		for (int i = 0; i < posts.size(); i++) {
			List<Tag> tags = dao.findTagsByPost(posts.get(i));
			posts.get(i).setTags(tags);
		}
		
		return posts;
	}
	
	public List<Tag> findAllTags(){
		List<Tag> tags = dao.findAllTags();
		for (int i = 0; i < tags.size(); i++) {
			List<Post> posts = dao.findPostByTag(tags.get(i));
			tags.get(i).setPosts(posts);
		}
		
		return tags;
	}

	@Override
	public void deletePost(Long id) {
		Post e = dao.getPostById(id);//e is the managed entity
	    dao.delete(e);
	    logger.info("Data POST "+e.getTitle()+ " Deleted.");
	}

	@Override
	public void deleteTag(Long id) {
		Tag e = dao.getTagById(id);
		dao.delete(e);
		logger.info("Data TAG "+e.getLabel()+ " Deleted.");
	}
}
