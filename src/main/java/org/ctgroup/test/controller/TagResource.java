package org.ctgroup.test.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.beanutils.BeanUtils;
import org.ctgroup.test.dto.PostDto;
import org.ctgroup.test.dto.TagDto;
import org.ctgroup.test.entity.Post;
import org.ctgroup.test.entity.Tag;
import org.ctgroup.test.services.ApplicationService;

@Path("/tag")
public class TagResource {
	
	@Inject
	ApplicationService applicationService;
	

	@GET
	@Path("/list")
	public List<TagDto> index() throws IllegalAccessException, InvocationTargetException {
		List<Tag> tags = applicationService.findAllTags();
		List<TagDto> tagDtos = new ArrayList<TagDto>();
		for (int i = 0; i < tags.size(); i++) {
			TagDto pd = new TagDto();
		    BeanUtils.copyProperties(pd, tags.get(i));
		    tagDtos.add(pd);
		}
		return tagDtos;
	}
	
	@POST
	@Path("/save")
	public Response save(TagDto tagDto) throws IllegalAccessException, InvocationTargetException {
		Tag tag = new Tag();
		BeanUtils.copyProperties(tag,tagDto);
		applicationService.save(tag);
		
		return Response
		        .status(Response.Status.CREATED)
		        .build();
	}
	
	@PUT
	@Path("/update")
	public Response update(TagDto tagDto) throws IllegalAccessException, InvocationTargetException {
		
		Tag tag = new Tag();
		BeanUtils.copyProperties(tag,tagDto);
		
		applicationService.updateTag(tag);
		
		return Response
		        .status(Response.Status.OK)
		        .build();
	}
	
	@DELETE
	@Path("/delete/{id}")
	public Response deleteTag(@PathParam("id") Long id) {
		applicationService.deleteTag(id);
		return Response
		        .status(Response.Status.OK)
		        .build();
	}
	
	
	
}
