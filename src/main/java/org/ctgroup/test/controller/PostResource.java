package org.ctgroup.test.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.beanutils.BeanUtils;
import org.ctgroup.test.dto.PostDto;
import org.ctgroup.test.entity.Post;
import org.ctgroup.test.services.ApplicationService;

@Path("/post")
public class PostResource {
	
	@Inject
	ApplicationService applicationService;
	

	@GET
	@Path("/list")
	public List<PostDto> index() throws IllegalAccessException, InvocationTargetException {
		List<Post> posts = applicationService.findAllPosts();
		List<PostDto> postDtos = new ArrayList<PostDto>();
		for (int i = 0; i < posts.size(); i++) {
			PostDto pd = new PostDto();
		    BeanUtils.copyProperties(pd, posts.get(i));
		    postDtos.add(pd);
		}
		return postDtos;
	}
	
	@POST
	@Path("/save")
	public Response save(PostDto postDto) throws IllegalAccessException, InvocationTargetException {
		Post post = new Post();
		BeanUtils.copyProperties(post,postDto);
		applicationService.save(post);
		
		return Response
		        .status(Response.Status.CREATED)
		        .build();
	}
	
	@PUT
	@Path("/update")
	public Response update(PostDto postDto) throws IllegalAccessException, InvocationTargetException {
		
		Post post = new Post();
		BeanUtils.copyProperties(post,postDto);
		
		applicationService.updatePost(post);
		
		return Response
		        .status(Response.Status.OK)
		        .build();
	}
	
	@DELETE
	@Path("/delete/{id}")
	public Response deletePost(@PathParam("id") Long id) {
		applicationService.deletePost(id);
		return Response
		        .status(Response.Status.OK)
		        .build();
	}
	
	
	
}
