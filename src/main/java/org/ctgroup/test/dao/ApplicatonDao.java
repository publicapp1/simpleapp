package org.ctgroup.test.dao;

import java.util.List;

import org.ctgroup.test.entity.Post;
import org.ctgroup.test.entity.Tag;

public interface ApplicatonDao {
	
	public void savePost(Post post);
	public List<Post> findAllPosts();
	public Post getPostById(Long id);
	public List<Tag> findTagsByPost(Post post);
	
	public void saveTag(Tag tag);
	public List<Tag> findAllTags();
	public Tag getTagById(Long id);

}
