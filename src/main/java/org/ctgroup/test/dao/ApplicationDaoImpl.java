package org.ctgroup.test.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.EntityManager;

import org.ctgroup.test.entity.Post;
import org.ctgroup.test.entity.Tag;
import org.hibernate.query.Query;

@ApplicationScoped
public class ApplicationDaoImpl implements ApplicatonDao{

	@Inject
    EntityManager entityManager;
	
	@Override
	public void savePost(Post post) {
		// TODO Auto-generated method stub
		entityManager.merge(post);
	}

	@Override
	public void saveTag(Tag tag) {
		// TODO Auto-generated method stub
		entityManager.persist(tag);
	}

	
	@Override
	public Post getPostById(Long id) {
		// TODO Auto-generated method stub
		return entityManager.find(Post.class, id);
	}

	@Override
	public List<Post> findAllPosts() {
		// TODO Auto-generated method stub
		Query query = (Query) entityManager.createQuery("select p from Post p", Post.class);
        return query.getResultList();
	}
	
	@Override
	public List<Tag> findAllTags() {
		// TODO Auto-generated method stub
		Query query = (Query) entityManager.createQuery("select t from Tag t", Tag.class);
        return query.getResultList();
	}

	@Override
	public Tag getTagById(Long id) {
		// TODO Auto-generated method stub
		return entityManager.find(Tag.class, id);
	}

	public List<Tag> findTagsByPost(Post post) {
		// TODO Auto-generated method stub
		Query query = (Query) entityManager.createQuery("SELECT t FROM Tag t JOIN FETCH t.posts p WHERE p = :post_id", Tag.class);
		query.setLong("post_id", post.getId());
		return query.getResultList();
	}

	public void merge(Post post) {
		// TODO Auto-generated method stub
		entityManager.merge(post);
	}
	
	public void merge(Tag tag) {
		entityManager.merge(tag);
	}

	public void delete(Post e) {
		// TODO Auto-generated method stub
		entityManager.remove(e);
	}
	
	public void delete(Tag e) {
		entityManager.remove(e);
	}

	public List<Post> findPostByTag(Tag tag) {
		// TODO Auto-generated method stub
		Query query = (Query) entityManager.createQuery("SELECT p FROM Post p JOIN FETCH p.tags t WHERE t = :tag_id", Post.class);
		query.setLong("tag_id", tag.getId());
		return query.getResultList();
	}
	
	

}
